package com.example.plugin.confluence.customcontent;

import com.atlassian.confluence.content.ContentEntityAdapter;
import com.atlassian.confluence.content.ContentType;
import com.atlassian.confluence.content.ui.ContentUiSupport;
import com.atlassian.confluence.content.ui.SimpleUiSupport;
import com.atlassian.confluence.security.PermissionDelegate;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.security.delegate.MailPermissionsDelegate;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

public class AwesomeContentType implements ContentType {

	private static final AwesomeContentEntityAdapter CONTENT_ADAPTER = new AwesomeContentEntityAdapter();

	private final PermissionDelegate permissionDelegate;
	private final ContentUiSupport contentUiSupport;

	public AwesomeContentType(final SpacePermissionManager spacePermissionManager,
			final WebResourceUrlProvider webResourceUrlProvider) {
		final MailPermissionsDelegate permissionDelegate = new MailPermissionsDelegate();
		permissionDelegate.setSpacePermissionManager(spacePermissionManager);
		this.permissionDelegate = permissionDelegate;
		contentUiSupport = new SimpleUiSupport(webResourceUrlProvider, "/images/icons/contenttypes/mail_16.png",
				"icon-mail", ContentTypesDisplayMapper.CSS_CLASS_PREFIX + "mail", "mail.name");
	}

	@Override
	public ContentEntityAdapter getContentAdapter() {
		return CONTENT_ADAPTER;
	}

	@Override
	public PermissionDelegate getPermissionDelegate() {
		return permissionDelegate;
	}

	@Override
	public ContentUiSupport getContentUiSupport() {
		return contentUiSupport;
	}
}
