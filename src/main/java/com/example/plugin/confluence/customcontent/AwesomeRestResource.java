package com.example.plugin.confluence.customcontent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.content.CustomContentEntityObject;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

@Path("/awesome")
@Produces(MediaType.APPLICATION_JSON)
public class AwesomeRestResource {

	private final AwesomeContentManager awesomeContentManager;
	private final TransactionTemplate transactionTemplate;

	public AwesomeRestResource(final AwesomeContentManager awesomeContentManager,
			final TransactionTemplate transactionTemplate) {
		this.awesomeContentManager = awesomeContentManager;
		this.transactionTemplate = transactionTemplate;
	}

	@PUT
	@Path("/{title}")
	public Response create(@PathParam(value = "title") final String title) {

		transactionTemplate.execute(new TransactionCallback() {
			@Override
			public Void doInTransaction() {
				awesomeContentManager.create(title);
				return null;
			}
		});

		return Response.ok().build();
	}

	@GET
	@Path("/all")
	public Response get() {

		final List<String> contents = new ArrayList<String>();

		final Iterator<CustomContentEntityObject> x = transactionTemplate
				.execute(new TransactionCallback<Iterator<CustomContentEntityObject>>() {
					@Override
					public Iterator<CustomContentEntityObject> doInTransaction() {
						return awesomeContentManager.get();
					}
				});

		while (x.hasNext()) {
			contents.add(x.next().getBodyAsString());
		}

		final List<CustomContentEntityObject> copy = new ArrayList<CustomContentEntityObject>();
		while (x.hasNext()) {
			copy.add(x.next());
		}

		final GenericEntity<List<String>> entity = new GenericEntity<List<String>>(contents) {
		};
		return Response.ok(entity).build();
	}
}
