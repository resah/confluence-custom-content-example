package com.example.plugin.confluence.customcontent;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.ContentEntityAdapterParent;
import com.atlassian.confluence.content.CustomContentEntityObject;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.fugue.Option;

public class AwesomeContentEntityAdapter extends ContentEntityAdapterParent {

	private static final Logger log = LoggerFactory.getLogger(AwesomeContentEntityAdapter.class);
	public static final String PLUGIN_CONTENT_KEY = "com.example.plugin.confluence.custom-content:awesome";

	@Override
	public Option<String> getUrlPath(final CustomContentEntityObject pluginContentEntityObject) {
		if (pluginContentEntityObject.getId() != 0 && pluginContentEntityObject.getSpace() != null) {
			return some("/display/" + pluginContentEntityObject.getSpace().getKey() + "/awesome/"
					+ pluginContentEntityObject.getId());
		}
		else {
			return none();
		}
	}

	@Override
	public Option<String> getDisplayTitle(final CustomContentEntityObject pluginContentEntityObject) {
		return none();
	}

	@Override
	public Option<String> getNameForComparison(final CustomContentEntityObject pluginContentEntityObject) {
		return none();
	}

	@Override
	public Option<String> getAttachmentsUrlPath(final CustomContentEntityObject pluginContentEntityObject) {
		return some(getUrlPath(pluginContentEntityObject).get() + "#attachments");
	}

	@Override
	public Option<String> getAttachmentUrlPath(final CustomContentEntityObject pluginContentEntityObject,
			final Attachment attachment) {
		return some(GeneralUtil.appendAmpsandOrQuestionMark(getUrlPath(pluginContentEntityObject).get()) + "highlight="
				+ GeneralUtil.urlEncode(attachment.getFileName()) + "#attachments");
	}

	@Override
	public BodyType getDefaultBodyType(final CustomContentEntityObject pluginContentEntityObject) {
		return BodyType.RAW;
	}

	@Override
	public Option<String> getExcerpt(final CustomContentEntityObject pluginContentEntityObject) {
		return Option.some(GeneralUtil.makeFlatSummary("excerpt"));
	}

}
