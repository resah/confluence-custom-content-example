package com.example.plugin.confluence.customcontent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.CustomContentEntityObject;

public class AwesomeContent extends CustomContentEntityObject {

	private static final long serialVersionUID = 3427945101602302069L;

	private static final Logger log = LoggerFactory.getLogger(AwesomeContent.class);

	private final CustomContentEntityObject content;

	public static boolean isAwesomeContentEntity(final CustomContentEntityObject contentEntityObject) {
		return AwesomeContentEntityAdapter.PLUGIN_CONTENT_KEY.equals(contentEntityObject.getPluginModuleKey());
	}

	public static AwesomeContent newInstance(final CustomContentEntityObject content) {
		if (content == null) {
			return null;
		}

		return new AwesomeContent(content);
	}

	private AwesomeContent(final CustomContentEntityObject content) {
		if (!isAwesomeContentEntity(content)) {
			throw new IllegalArgumentException("Object " + content + " is not a Awesome content object.");
		}

		this.content = content;
	}

	@Override
	public CustomContentEntityObject getEntity() {
		return content;
	}

	public String getAwesomeId() {
		return content.getProperties().getStringProperty("messageId");
	}

	public String getAwesomeContent() {
		return content.getBodyAsString();
	}

	@Override
	public String getSpaceKey() {
		return getEntity().getSpaceKey();
	}

}
