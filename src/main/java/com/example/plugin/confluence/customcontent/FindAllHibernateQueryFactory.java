package com.example.plugin.confluence.customcontent;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Query;
import net.sf.hibernate.Session;

import com.atlassian.confluence.content.persistence.hibernate.HibernateContentQueryFactory;

/**
 */
public class FindAllHibernateQueryFactory implements HibernateContentQueryFactory {
	@Override
	public Query getQuery(final Session session, final Object... parameters) throws HibernateException {
		final Query query = session
				.createQuery("from CustomContentEntityObject content where content.originalVersion is null order by content.creationDate asc");
		return query;
	}
}
