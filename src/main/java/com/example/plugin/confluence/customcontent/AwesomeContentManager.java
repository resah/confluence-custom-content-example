package com.example.plugin.confluence.customcontent;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.ContentQuery;
import com.atlassian.confluence.content.CustomContentEntityObject;
import com.atlassian.confluence.content.CustomContentManager;
import com.atlassian.confluence.core.DefaultSaveContext;

public class AwesomeContentManager {

	private static final Logger log = LoggerFactory.getLogger(AwesomeContentManager.class);

	private final CustomContentManager customContentManager;

	public AwesomeContentManager(final CustomContentManager customContentManager) {
		this.customContentManager = customContentManager;
	}

	public void create(final String content) {

		final CustomContentEntityObject entity = customContentManager
				.newPluginContentEntityObject(AwesomeContentEntityAdapter.PLUGIN_CONTENT_KEY);
		entity.setPluginModuleKey(AwesomeContentEntityAdapter.PLUGIN_CONTENT_KEY);
		entity.setBodyAsString(content);

		customContentManager.saveContentEntity(entity, new DefaultSaveContext(false, true, false));

	}

	public Iterator<CustomContentEntityObject> get() {
		final ContentQuery<CustomContentEntityObject> x = new ContentQuery<CustomContentEntityObject>("awesome.findAll");
		return customContentManager.findByQuery(x, 0, 100);

	}

}
